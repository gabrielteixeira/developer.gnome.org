# Writing Style

The following conventions should be followed when writing for the developer docs.

## Guidelines

 - Documentation should be written using US English.
 - Page headings should be written using title capitalization. All other headings should use sentence capitalization.

## Other Style Guides

 - [Red Hat](https://stylepedia.net/)
 - [Microsoft](https://docs.microsoft.com/en-us/style-guide/)
